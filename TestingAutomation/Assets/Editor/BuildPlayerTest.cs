﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class BuildPlayerTest: MonoBehaviour 
    {

        [MenuItem("Build/Build iOS")]
        public static void MyBuild()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.scenes = new[] { "Assets/testscene.unity"};
            buildPlayerOptions.locationPathName = "iOSBuild";
            buildPlayerOptions.target = BuildTarget.iOS;
            buildPlayerOptions.options = BuildOptions.None;

            var report = BuildPipeline.BuildPlayer(buildPlayerOptions);
             Debug.Log(report);
        }
    }
}
